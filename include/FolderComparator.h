#ifndef __FILE_COMPARATOR_H__
#define __FILE_COMPARATOR_H__

#include <vector>
#include <map>
#include <mutex>

#include <boost/filesystem.hpp>
#include <boost/multiprecision/cpp_int.hpp>

//#define DEBUG

using namespace boost::filesystem;
using namespace boost::multiprecision;

struct file_desc {
   path Path;
   uint128_t Weight;
   bool eof;
};

typedef std::map<uint128_t, std::vector<file_desc>> semiready;

inline bool operator < (const file_desc &a, const file_desc &b)
{
    return a.Weight < b.Weight;
}

inline bool operator == (const file_desc &a, const file_desc &b)
{
    return a.Weight == b.Weight;
}

class FolderComparator
{
protected:
    path root1, root2;

public:
    FolderComparator(const path &p1, const path &p2):root1(p1),root2(p2) {}
    virtual ~FolderComparator() {}

    virtual int findSimilar(semiready &smr_result)=0;
};

class FolderComparatorImpl:public FolderComparator
{
public:
    FolderComparatorImpl(const path &p1, const path &p2):FolderComparator(p1, p2), result_idx(0) {}
    ~FolderComparatorImpl() {}
    int findSimilar(semiready &smr_result);

protected:
    virtual uint128_t hash(uint8_t *buffer, size_t buf_size);

private:
    const size_t BLK_SIZE = 4096;

    std::mutex result_mtx;
    uint128_t result_idx;

    int  loadTree(std::vector<file_desc> &storage, const path &p, const int n=0);
    void filterByLength(std::vector<file_desc> &storage, semiready &smr);
    void filterByBlockHash(semiready &smr_in, semiready &smr_out, semiready &smr_result, uint32_t block_num);

};

#define FC_OK                   0
#define FC_FILE_DOES_NOT_EXIST -1
#define FC_BAD_DIR_NAME        -2

#endif // __FILE_COMPARATOR_H__

