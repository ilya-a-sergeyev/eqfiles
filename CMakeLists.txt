cmake_minimum_required(VERSION 3.0)
project(eqfiles)

file(GLOB_RECURSE sources src/*.cpp)

add_executable(eqfiles ${sources})

target_compile_options(eqfiles PUBLIC -std=c++14 -g -O2 -Wall -Wfloat-conversion -Wno-deprecated -fPIC)
target_include_directories(eqfiles PUBLIC src include)

find_package(Boost 1.62.0 COMPONENTS filesystem system REQUIRED)

target_link_libraries(eqfiles PUBLIC
  ${Boost_LIBRARIES}
  pthread
  ssl
  crypto
)

