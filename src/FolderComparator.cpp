#include "FolderComparator.h"

#include <stdio.h>
#include <string.h>
#include <openssl/md5.h>

#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <map>
#include <thread>
#include <mutex>

#include <boost/filesystem.hpp>
#include <boost/multiprecision/cpp_int.hpp>

using namespace boost::filesystem;
using namespace boost::multiprecision;

int FolderComparatorImpl::loadTree(std::vector<file_desc> &storage, const path &p, const int n)
{
  int dir_count=0;
  int file_count=0;
  int tree_depth=0;

  if (exists(p))
  {
    if (tree_depth<n)
      tree_depth=n;
    if (is_directory(p)) {
      for (auto entry:directory_iterator(p)) {
        const path &pp = entry.path();
        // skip symlinks to avoid loops
        if (symlink_status(pp).type() == symlink_file)
          continue;
        if (is_directory(entry)) {
            if (pp == ".." || pp == ".")
              continue;
            ++dir_count;
            loadTree(storage, pp, n+1);
        }
        if (boost::filesystem::status(pp).type() == regular_file) {
          ++file_count;
          file_desc dsc;
          dsc.Path = std::move(pp);
          dsc.Weight = boost::filesystem::file_size(pp);
          dsc.eof = false;
          storage.push_back(dsc);
        }
        continue;
      }
    }
    else {
        return FC_BAD_DIR_NAME;
#ifdef DEBUG
      std::cout << p << " exists, but is not a directory\n";
#endif
    }
  }
  else {
#ifdef DEBUG
    std::cout << p << " does not exist\n";
#endif
    return FC_FILE_DOES_NOT_EXIST;
  }
#ifdef DEBUG
    std::cout << "\nTotal " << storage.size() << " files.\n";
    std::cout << dir_count << " directories.\n";
    std::cout << "Tree depth: " << tree_depth << "\n\n";
#endif
    return FC_OK;
}

void FolderComparatorImpl::filterByLength(std::vector<file_desc> &storage, semiready &smr)
{
    // ToDo: change to move
    for (auto it=storage.begin(); it != storage.end(); ++it)
    {
        smr[(*it).Weight].push_back(*it);
    }
    storage.clear();

    // remove all uniques
    auto kv =  smr.begin();
    while (kv != smr.end()) {
        if ((*kv).second.size()<2) {
            kv = smr.erase(kv);
        }
        else {
            kv++;
        }
    }
#ifdef DEBUG
    std::cout << "\n";
    for (auto &kv: smr) {
        std::cout << kv.first << "\n";
        for (auto rdf: kv.second) {
          std::cout << "Found semi-ready: " << rdf.Path << "\n";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
#endif
}

uint128_t FolderComparatorImpl::hash(uint8_t *buffer, size_t buf_size)
{
    uint128_t md5_digest;

    if (!buffer || !buf_size)
        return 0;

    MD5(buffer, buf_size, (unsigned char*)&md5_digest);

    return md5_digest;
}

void FolderComparatorImpl::filterByBlockHash(semiready &smr_in, semiready &smr_out, semiready &smr_result, uint32_t block_num)
{
    uint128_t md5_digest;
    uint8_t *buffer = new uint8_t[BLK_SIZE];

    if (!buffer)
        return;

    // calculate hash's
    // can be parallelized
    for (auto &kv: smr_in) {
        for (file_desc &it: kv.second) {
            FILE *fh = fopen(it.Path.c_str(),"rb");
            if (fh) {
                size_t rd_cnt = 0;
                memset(buffer, 0, BLK_SIZE);
                if (fseek(fh, block_num*BLK_SIZE, SEEK_SET) ==  0)
                    rd_cnt = fread(buffer, 1, BLK_SIZE, fh);
                // ToDo: make it virtual function
                if (rd_cnt) {
                  it.Weight ^= hash(buffer, BLK_SIZE);
                }
                if (rd_cnt < BLK_SIZE) {
                    it.eof = true;
                }
                smr_out[it.Weight].push_back(it);
                fclose(fh);
            }
        }
    }

    // remove all uniques
    // can be parallelized
    auto kv =  smr_out.begin();
    while (kv != smr_out.end()) {
        if ((*kv).second.size()<2) {
            kv = smr_out.erase(kv);
        }
        else {
            std::lock_guard<std::mutex> lock(result_mtx);
            // find and remove from src finally equal files (eof & equal hash) or single in the group file
            // and place them into result storage
            std::vector<file_desc> &src = (*kv).second;
            std::vector<file_desc> &dst = smr_result[result_idx];
            auto p = std::stable_partition(src.begin(), src.end(), [&](const file_desc& x) { return !x.eof; });
            dst.insert(dst.end(), std::make_move_iterator(p), std::make_move_iterator(src.end()));
            src.erase(p, src.end());
            // clear result group if it has only 1 file - it's unique
            if (smr_result[result_idx].size()==1) {
                smr_result[result_idx].clear();
            }
            else {
                result_idx++;
            }
            if ((*kv).second.size()<2) {
                kv = smr_out.erase(kv);
            }
            else {
                kv++;
            }
        }
    }

    delete buffer;

#ifdef DEBUG
    std::cout << "\n";
    for (auto &kv: smr_out) {
        std::cout << kv.first << "\n";
        for (auto rdf: kv.second) {
          std::cout << "Found semi-ready: " << rdf.Path << "\n";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
#endif
}

int FolderComparatorImpl::findSimilar(semiready &smr_result)
{
    std::vector<file_desc> storage;
    semiready smr_in;
    semiready smr_out;
    result_idx = 0;

    int err_code = FC_OK;

    do {
        // 0. load filenames
        err_code = loadTree(storage, root1, 0);
        if (err_code)
            break;

        err_code = loadTree(storage, root2, 0);
        if (err_code)
            break;

        // 1. filter files with unique filesize
#ifdef DEBUG
        std::cout << "\nIteration: 0 (size)\n";
#endif
        filterByLength(storage, smr_in);

        // 2. filter files with unique MD5 sum of 4096 data block
        int block_num = 0;
        while (smr_in.size()) {
#ifdef DEBUG
          std::cout << "\nIteration: " << block_num+1 << " (block hash #" << block_num << ")\n";
#endif
          filterByBlockHash(smr_in, smr_out, smr_result, block_num);
          smr_in = smr_out;
          smr_out.clear();
          block_num++;
        };
    } while (0);

    return err_code;
}

