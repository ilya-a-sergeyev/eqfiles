#include <iostream>
#include "FolderComparator.h"

int main(int argc, char* argv[])
{
    int err_code = 0;

    do {
        semiready smr_result;

        if (argc < 3)
        {
          std::cout << "Usage: eqfiles path_1 path_2\n";
          err_code = 1;
          break;
        }

        FolderComparator *fc = new FolderComparatorImpl(argv[1], argv[2]);

        if (!fc) {
            std::cout << "Allocation error\n";
            err_code = 2;
            break;
        }

        err_code = fc->findSimilar(smr_result);

        if (err_code != FC_OK) {
            std::cout << "Search error : " << err_code << "\n";
            break;
        }

        std::cout << "\nResult:\n";
        for (auto &kv: smr_result) {
            if (kv.second.size()) {
                std::cout << kv.first << "\n";
                for (auto rdf: kv.second) {
                  std::cout << "Found: " << rdf.Path << "\n";
                }
                std::cout << "\n";
            }
        }
        std::cout << "\n";

        delete fc;
    } while (0);

    return err_code;
}
